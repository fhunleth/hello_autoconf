defmodule Mix.Tasks.Compile.Autoconf do
  use Mix.Task

  @recursive true

  @moduledoc """
  Runs Automake tools in the current project.

  This task runs `autoconf` in the current project.

  ## Configuration

  This compiler can be configured through the return value of the `project/0`
  function in `mix.exs`; for example:

      def project() do
        [app: :myapp,
         compilers: [:autoconf] ++ Mix.compilers,
         deps: deps()]
      end

  The following options are available:

    * `:autoconf_cwd` - (binary) it's the directory where `make` will be run,
      relative to the root of the project.

    * `:autoconf_configure_env` - (map of binary to binary) it's a map of extra environment
      variables to be passed to `configure`.

    * `:autoconf_error_message` - (binary or `:default`) it's a custom error message
      that can be used to give instructions as of how to fix the error (e.g., it
      can be used to suggest installing `gcc` if you're compiling a C
      dependency).

  """

  @unix_error_msg """
  Depending on your OS, make sure to follow these instructions:

    * Mac OS X: TBD - what should be installed?

    * Linux: TBD - is this in build-essential?
      You need to have gcc and make installed. If you are using
      Ubuntu or any other Debian-based system, install the packages
      "build-essential".
  """

  @windows_error_msg ~S"""
  I'm not sure how well autoconf projects will run on Windows???
  """

  @spec run(OptionParser.argv) :: :ok | no_return
  def run(args) do
    config = Mix.Project.config()
    Mix.shell.print_app()
    build(config, args)
    Mix.Project.build_structure()
    :ok
  end

  defp build(config, task_args) do
    cwd = Keyword.get(config, :autoconf_cwd, ".") |> Path.expand(File.cwd!())
    autoreconf = Keyword.get(config, :autoconf_autoreconf, false)
    configure = Keyword.get(config, :autoconf_configure, Path.join(cwd, "configure"))
    makefile = Keyword.get(config, :autoconf_makefile, Path.join(cwd, "Makefile"))
    autoreconf_env = Keyword.get(config, :autoconf_autoreconf_env, %{})
    autoreconf_args = Keyword.get(config, :autoconf_autoreconf_args, ["--install"])
    configure_env = Keyword.get(config, :autoconf_configure_env, %{})
    configure_args = Keyword.get(config, :autoconf_configure_args, [])

    error_msg = Keyword.get(config, :autoconf_error_message, :default) |> os_specific_error_msg()
    verbose? = "--verbose" in task_args

    if autoreconf && !File.exists?(configure) do
      exec = find_executable("autoreconf")
      cmd!(exec, autoreconf_args, cwd, autoreconf_env, verbose?, error_msg)
    end

    if !File.exists?(makefile) do
      exec = Path.join(cwd, "configure")
      cmd!(exec, configure_args, cwd, configure_env, verbose?, error_msg)
    end
  end

  defp cmd!(exec, args, cwd, env, verbose?, error_msg) do
    case cmd(exec, args, cwd, env, verbose?) do
      0 ->
        :ok
      exit_status ->
        raise_build_error(exec, exit_status, error_msg)
    end
  end
  # Runs `exec [args]` in `cwd` and prints the stdout and stderr in real time,
  # as soon as `exec` prints them (using `IO.Stream`).
  defp cmd(exec, args, cwd, env, verbose?) do
    opts = [
      into: IO.stream(:stdio, :line),
      stderr_to_stdout: true,
      cd: cwd,
      env: env
    ]

    if verbose? do
      print_verbose_info(exec, args)
    end

    {%IO.Stream{}, status} = System.cmd(exec, args, opts)
    status
  end

  defp find_executable(exec) do
    System.find_executable(exec) || Mix.raise("""
    "#{exec}" not found in the path. Please check that the autotools package
    is installed.
    """)
  end

  defp raise_build_error(exec, exit_status, error_msg) do
    Mix.raise(~s{Could not compile with "#{exec}" (exit status: #{exit_status}).\n} <> error_msg)
  end

  defp os_specific_error_msg(msg) when is_binary(msg) do
    msg
  end

  defp os_specific_error_msg(:default) do
    case :os.type() do
      {:unix, _} -> @unix_error_msg
      {:win32, _} -> @windows_error_msg
      _ -> ""
    end
  end

  defp print_verbose_info(exec, args) do
    args = Enum.map_join(args, " ", fn(arg) ->
      if String.contains?(arg, " "), do: inspect(arg), else: arg
    end)

    Mix.shell.info "Compiling with autoconf: #{exec} #{args}"
  end
end
